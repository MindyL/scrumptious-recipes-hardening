from tkinter import CASCADE
from django.db import models
from django.conf import settings

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="mealplans",
        on_delete=models.CASCADE,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="mealplans")

    def __str__(self):
        return f"{self.name} for {self.owner} "
