from django import template

register = template.Library()


def resize_to(ingredient, target):
    original_servings = ingredient.recipe.servings
    if original_servings is not None and target is not None:
        try:
            ratio = float(target) / original_servings
            return ratio * ingredient.amount
        except:
            pass

    return ingredient.amount


register.filter(resize_to)
